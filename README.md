# TS Express GraphQL

Plugin extension to quickly start a graphQL server

**//TODO:**
-[ ] Watch and reload files
-[ ] Allow Configuration for plugins

## Install

```sh
npm i @rippell/ts-express-graphql
```

---

Create and/or add `tsExpressGraphql` to your `tsexpress.ts` config file at the top level of your project:

```typescript
import { TSExpressConfig } from '@rippell/ts-express';
import { tsExpressGraphql } from '@rippell/ts-express-graphql';

export const config: TSExpressConfig = {
  plugins: [ tsExpressGraphql ],
  verbose: true
}
```

---

Create Schema files named `*.graphql` anywhere under your src folder.
All will be found and concatenated -- be sure to create one `Query` and `Mutation` and extend in all other files.

```graphql
# File: some.graphql

type Query {
  test: String!
}

type Mutation {
  testing(id: ID): String
}
```

```graphql
# File: other.graphql

extend type Query {
  moarTest: String!
}
``` 

---

Create [Resolvers](https://www.apollographql.com/docs/graphql-tools/resolvers/) anywhere in your `src` folder named with any of the following extensions: `*.resolver.ts`, `*.query.ts`, or `*.mutation.ts`.

If any of these files should export a `resolvers` object.
All found objects are merged to build up the full master resolver.

```typescript
// File: some.resolver.ts 

import { IResolvers } from 'graphql-tools';

export const resolvers: IResolvers = {
  Query: {
    test(parent, args): string {
      return 'Test!' ;
    },
  },
  Mutation: {
    testing: (parent, args) => {
      return 'Mutate ' + args.id;
    }
  },
  Custom: {
    stuff: (parent, args) => {
      return 'Resolve for Custom.stuff'
    }
  }
};
```

```typescript
// File: some.query.ts 

import { IResolvers } from 'graphql-tools';

export const resolvers: IResolvers = {
  Query: {
    test(parent, args): string {
      return 'Another file!?' ;
    }
  }
};
```
