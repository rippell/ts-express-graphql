import * as path from 'path';
import { glob } from 'glob';
import { Express } from 'express';
import 'graphql-import-node';
import { makeExecutableSchema, IResolvers } from 'graphql-tools';
import { GraphQLSchema } from 'graphql';
import { ApolloServer } from 'apollo-server-express';
import * as depthLimit from 'graphql-depth-limit';
import { TSExpressConfig } from '@rippell/ts-express';

export const tsExpressGraphql = (app: Express, config: TSExpressConfig, cb: any) => {
  config.log('\n=== GraphQL Plugin ===');

  glob(path.join(config.dir, 'src/**/*.graphql'), { cache: false }, (er, files) => {
    const schemas = files.reduce((s, file) => {
      config.log('Discovered ' + file.substr(config.dir.length));
      s.push(require(file));
      return s;
    }, []);

    glob(path.join(config.dir, 'src/**/*.+(resolver|query|mutation).ts'), { cache: false }, (e, resolverFiles) => {
      const resolvers = resolverFiles.reduce((master, resolverFile) => {
        const exports = require(resolverFile);
        if (exports.resolvers) {
          Object.keys(exports.resolvers).forEach(r => {
            master[r] = Object.assign(master[r] || {}, exports.resolvers[r]);
          });
        }
        return master;
      }, { Query: {}, Mutation: {}} as IResolvers);

      config.log('\n--- Resolvers ---');
      config.log(resolvers);

      const schema: GraphQLSchema = makeExecutableSchema({
        typeDefs: schemas,
        resolvers,
      });

      const server = new ApolloServer({
        schema,
        validationRules: [depthLimit(7)],
      });

      server.applyMiddleware({ app, path: '/graphql' });

      config.log('XXX GraphQL Plugin XXX\n');
      cb();
    });

  });
}
